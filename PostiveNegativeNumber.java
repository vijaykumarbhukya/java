import java.util.Scanner;
public class PostiveNegativeNumber {

	
	    public static void main(String[] args) 
	    {
	        int n;
	        Scanner input = new Scanner(System.in);
	        System.out.print("Enter the number you want to check:");
	        n = input.nextInt();
	        if(n > 0)
	        {
	            System.out.println("The given number "+n+" is Positive");
	        }
	        else if(n < 0)
	        {
	            System.out.println("The given number "+n+" is Negative");
	        }
	        else
	        {
	            System.out.println("The given number "+n+" is neither Positive nor Negative ");
	        }
	    }
	}
